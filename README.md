# QEMU for EEUM CXL ecosystem

## Building

### Install QEMU build dependencies

```
sudo apt-get install git libglib2.0-dev libfdt-dev libpixman-1-dev zlib1g-dev \
  ninja-build libslirp-dev cloud-image-utils
```

### Build QEMU

```
# Checkout code from EEUM's repository
git clone git@gitlab.com:eeum-public/qemu.git
cd qemu

# Create build directory
mkdir build
cd build

# Configure and build
../configure --target-list=x86_64-softmmu --enable-debug --enable-slirp
make -j$(nproc)
```

### Setting up QEMU images

#### Download images

```
wget https://download.fedoraproject.org/pub/fedora/linux/releases/39/Cloud/x86_64/images/Fedora-Cloud-Base-39-1.5.x86_64.qcow2
```
```
wget https://eeum.s3.us-west-1.amazonaws.com/public/qemu-images/fedora_39.qcow2
```

The downloaded QEMU image is already patched with a custom kernel, so you don't need to build a custom kernel.


#### Create guest OS image (Alternative)

Please try the following steps if you would like to set up your own QEMU image.
You may skip this section if you want to use the download Fedora image instead.

```
wget https://download.fedoraproject.org/pub/fedora/linux/releases/39/Cloud/x86_64/images/Fedora-Cloud-Base-39-1.5.x86_64.qcow2
qemu-img create -b Fedora-Cloud-Base-39-1.5.x86_64.qcow2 -f qcow2 -F qcow2 fedora_39.qcow2 10G
```

#### Create cloud init image

Create userdata.yaml file using the following:

```yaml
#cloud-config
hostname: cxl-fedora
disable_root: false
ssh_pwauth: true
users:
  - name: eeum
    plain_text_passwd: eeum
    lock_passwd: false
    groups: wheel
    shell: /bin/bash
```

```
cloud-localds -v seed.qcow2 userdata.yaml
```

## Starting QEMU

`qemu/cxl_scripts` directory has preconfigured scripts for starting the QEMU for CXL emulator.

**Please start CXL emulator first. Otherwise QEMU will hang.**


```
cd ../cxl_scripts
./start-vm.sh
```

When prompted for username and password, use `eeum`/`eeum`.

## Building Custom Kernel

Creating CXL region requires enabling a Linux kernel config, `CONFIG_CXL_REGION_INVALIDATION_TEST`.

You can get the Linux kernel source code for Fedora 39 and build a custom kernel using the following steps.

1. Upgrade packages.

    `dnf upgrade`
1. Reboot.
1. Install dependencies.

    `dnf install fedpkg fedora-packager rpmdevtools ncurses-devel pesign grubby`
1. Get kernel source.

    `fedpkg clone -a kernel`
1. Get source for a specific version. The example will checkout `f39`.

    `cd kernel; git switch f39`
1. Get additional dependencies.

    `dnf builddep kernel.spec`
1. Get sources.

    `fedpkg sources`
1. Extract sources. In this example, the kernel version is 6.6.8.

    `tar -xf linux-6.6.8.tar.xz`
1. Move to the source directory.

    `cd linux-6.6.8`
1. Copy installed kernel config.

    `cp /boot/config-$(uname -r)* .config`
1. Open .config and update `CONFIG_CXL_REGION_INVALIDATION_TEST`.

    Set CONFIG_CXL_REGION_INVALIDATION_TEST=y
1. Build kernel.

    `make -j $(nproc)`
1. Install kernel modules

    `make modules_install`
1. Install kernel.

    `make install`
1. Reboot.

## Creating CXL Regions and Accessing CXL Memory

### Create CXL Regions

After booting up QEMU, CXL devices attached the guest OS are not enabled, and we need to create
a CXL region to access those CXL devices.

1. Install cxl-cli and daxctl.

    `dnf install cxl-cli daxctl -y`
1. Create a CXL region.

    `cxl create-region -m -d decoder0.0 mem0 mem1 mem2 mem3`
1. Run `cxl list` to confirm a CXL region is created successfully
1. Run `cat /proc/iomem` to confirm region and dax device are created successfully.

### Load CXL region using devdax driver.

The created region can be accessed using a dax device when dax device is loaded as a character device.
We need to reconfigure dax device so that it will not be a System RAM.

`daxctl reconfigure-device --human --mode=devdax --force dax0.0`

Run `daxctl list` and check `mode` is `devdax`.

### Build IO Generator

IO Generator source code is located under cxl_scripts/iogen.c

1. Create a new directory.

    `mkdir ~/iogen`
1. Move to iogen directory.

    `cd ~/iogen`
1. Create `iogen.c`.

    `vi iogen.c`
1. Copy contexts from `cxl_scripts/iogen.c` into the new file.
1. Compile

    `gcc iogen.c -o iogen`

Once IO generator is successfully compiled, you can start sending CXL mem read/write traffics by running `./iogen`.
